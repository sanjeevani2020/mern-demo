import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';


import Routes from './Routes';
import UserProvider from './contexts/UserConrtext';
import { Login, Signup } from './pages';
import { BlankLayout } from './layouts';
import Utils from './Utils';

const App = (props) => {
  const RoutedLayout = () => {
    <div className='h-100'>


      {/* {Routes.map((route, index) => {
        return <Route
          key       = {index}
          path      = {route.path}
          exact     = {route.exact}
          component = {(props => {
              return(
                <route.layout {...props}>
                  <route.component {...props} />
                </route.layout>
              )
            }
          )}        
        />

      })} */}
    </div>
  }

  // const Provider = () => {
  //   return <UserProvider>
  //       <RoutedLayout />
  //   </UserProvider>
  // }

  // return <Provider />;

  return <UserProvider >
    <Signup />
  </UserProvider>
}

export default App;
