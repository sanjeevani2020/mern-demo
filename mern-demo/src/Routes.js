import React from 'react';

import {Login, Signup, AddTech} from './pages';
import { BlankLayout } from './layouts';

const Routes = [

    {
        'path'      : '/login',
        'exact'     : true,
        'secure'    : true,
        'layout'    : BlankLayout,
        'component' : Login
    },

    {
        'path'      : '/signup',
        'exact'     : true,
        'secure'    : true,
        'layout'    : BlankLayout,
        'component' : Signup
    },

    {
        'path'      : '/addtech',
        'exact'     : true,
        'secure'    : true,
        'layout'    : BlankLayout,
        'component' : AddTech
    }


]

export default Routes;