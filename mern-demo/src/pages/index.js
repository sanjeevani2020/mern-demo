import Signup from "./Signup";
import Login from "./Login";
import AddTech from "./AddTech";

export {Signup, Login, AddTech};