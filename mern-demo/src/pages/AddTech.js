import React, { useState, useEffect } from "react";
import { Form } from "react-router-dom";
import axios from "axios";

const AddTech = () => {

    const [file, setFile] = useState();
    const [fileName, setFileName] = useState("");


    const uploadFile = (e) => {
        const formData = new FormData();
        formData.append("file", file);
        formData.append("fileName", fileName);
        axios.post("http://localhost:3009/upload", formData)
            .then(res => {
                console.log("File uploaded : " ,res)
            })
            .catch(err =>  console.log("Error : ", err));

    }

    const saveFile = (e) => {
        console.log(e.target.files[0], " target")
        setFile(e.target.files[0]);
        setFileName(e.target.files[0].name);
    }

    return <>
    <form>
      <label>Enter your name:
        <input
          type="file" 
          onChange={saveFile}
        />
        <button onClick = {uploadFile}> Upload File </button>
      </label>
    </form>
    </>
}

export default AddTech;