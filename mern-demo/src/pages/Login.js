import React, {useState, useEffect, useContext} from 'react';
import UserContext from '../contexts/UserConrtext';

const Login = ( props ) => {

    const [userData, setUserData] =  useState({email : '', password : '', role : ''});
    const {login} = useContext(UserContext);

    const handleLogin = (e) => {
        let evt = e.currentTarget;
        setUserData({
            email       : evt.email.value,
            password    : evt.password.value,
            role        : evt.role.value
        })

        login(userData);

    }

    const handleCallback = (data) => {
        console.log("callback " , data);
    }

    return <>
        <div className="container">
            <div className="wrapper">
                <form className="p-3 mt-3" onSubmit={handleLogin}>
                    <div className="form-field d-flex align-items-center">
                        <span className="fas fa-key"></span>
                        <input type="text" name="email" id="email" placeholder="Email" />
                    </div>
                    <div className="form-field d-flex align-items-center">
                        <span className="fas fa-key"></span>
                        <input type="password" name="password" id="pwd" placeholder="Password" />
                    </div>
                    <div className="form-field d-flex align-items-center">
                        <select  name="role">
                            <option>Mentor</option>
                            <option>Employee</option>
                        </select>
                    </div>
                    <button className="btn mt-3  signup" type="submit"> Login</button>
                </form>
            </div>
        </div>
    </>
}

export default Login;