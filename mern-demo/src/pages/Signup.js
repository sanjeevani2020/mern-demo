import axios from "axios";
import React, {useState, useEffect, useContext} from "react";
import UserContext from "../contexts/UserConrtext";

const Signup = () => {

    const [userData, setUserData] = useState({uname: '', email: '', pass: '', role: '' });
    const {signup} = useContext(UserContext);

    const handleSignup = (e) => {
        let evt = e.currentTarget;
        setUserData({
            uname       : evt.userName.value,
            email       : evt.email.value,
            password    : evt.password.value,
            role        : evt.role.value   
        })

        signup(userData);

    }
    const handleCallback = (data) => {
        window.location.href = process.env.PUBLIC_URL + '/login';
    }

    return <>

        <div className="container">
            <div className="wrapper">
                <form className="p-3 mt-3" onSubmit={handleSignup}>
                    <div className="form-field d-flex align-items-center">
                        <span className="far fa-user"></span>
                        <input type="text" name="userName" id="userName" placeholder="Username" />
                    </div>
                    <div className="form-field d-flex align-items-center">
                        <span className="fas fa-key"></span>
                        <input type="text" name="email" id="email" placeholder="Email" />
                    </div>
                    <div className="form-field d-flex align-items-center">
                        <span className="fas fa-key"></span>
                        <input type="password" name="password" id="pwd" placeholder="Password" />
                    </div>
                    <div className="form-field d-flex align-items-center">
                        <select  name="role">
                            <option>Mentor</option>
                            <option>Employee</option>

                        </select>
    
                    </div>
                    <button className="btn mt-3  signup" type="submit"> Sign Up</button>
                </form>
            </div>
        </div>

    </>
}

export default Signup;
