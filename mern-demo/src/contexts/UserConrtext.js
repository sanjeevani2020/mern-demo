import axios from 'axios';
import React, { createContext } from 'react';
import Utils from '../Utils';

const UserContext = createContext();

const UserProvider = (props) => {

    const signup = (data) => {
        axios.post(`${process.env.REACT_API_URL}signup`, data, Utils.apiHeaders())
            .then(res => console.log("Signup res : ", res))
            .catch(err => console.log("Signup Error : ", err));
    }

    const login =  (data) => {
        axios.post(`${process.env.REACT_API_URL}login`, data, Utils.apiHeaders())
            .then(res =>  console.log(res))
            .catch(err =>  console.log(err));
    }

    return (<UserContext.Provider value={{signup, login}}>
        {props.children}
    </UserContext.Provider>
    );
}

export default UserContext;

export {UserProvider};