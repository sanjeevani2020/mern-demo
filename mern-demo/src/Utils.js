import _ from 'lodash';

const Utils = {
    apiUrl : (path) => {
        let url = process.env.REACT_API_URL  + path;
        return url;
    },

    apiHeaders : () => {
        let headers = {
            "headers" : {
                "Content-Type": "application/json;charset=UTF-8",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "PUT, GET, POST, DELETE, OPTIONS, PATCH",
                'Accept': 'application/json'
            }
        }
    },

    setUserData : (data) => {
        const userData = _.get(data, 'token', false);
        if(userData === false){
            localStorage.removeItem(process.env.REACT_APP_APPNAME + 'userData');
        }else{
            localStorage.setItem(process.env.REACT_APP_APPNAME + 'userData' , JSON.stringify(data));
        }
    },

    getUserData : () => {
        let userData = localStorage.getItem(process.env.REACT_APP_APPNAME + 'userData');
        if(userData) return JSON.parse(userData);        
        else return false;
    },

    isLoggedIn : () => {
        const data = Utils.getUserData();
        return _.get(data, 'token', false) != false;
    }

}

export default Utils;