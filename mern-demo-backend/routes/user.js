const express       = require('express');
const router        = express.Router();
const _             = require('lodash');
const path          = require('path');

const db            = require('../DB/DB');
const dbConn        = new db();


module.exports = () => {

    router.get('/login', (req, res) => {
        
        dbConn.login(req.query)
            .then(result => res.send(result))
            .catch(err => res.send(err));
    })


    router.post('/signup', (req, res) => {
        console.log(req.body , " req")
        dbConn.signup(req.body)
            .then(result => res.send(result))
            .catch(err => res.send(err));
    })

    router.post('/upload', (req, res) => {
        console.log("  files", req.files.file.files  )
            const newPath   = path.join(__dirname, '..', "/uploads/images/");
            const file      = req.files.file.files;
            const filename  = file.name;

            file.mv(`${newPath}${filename}`, (err) => {
                if(err) res.status.send(500).send({message : "File upload failed", code: 200});
                res.status(500).send({message: "File Uploaded", code: 200});
            })

    })


    return router;
}
