const jwt   = require('jsonwebtoken');
const _     = require('lodash');


const genToken = (payload) => {
    try{
        const token = jwt.sign(payload, process.env.JWTSECRET, {algorithm : 'HS256'});
        return token;
    }
    catch(err){
        console.log("Token generation error : " , err);
        return err;
    }
}

const verifyToken = (req) => {
    const ret = { success : false, message : "Invalid Access!"};

    try{
        const token = req.header('token');
        if(token && !_.isNull(token) && !_.isEmpty(token)){
            let userData = jwt.verify(token, process.env.JWTSECRET);
            if(_.get(userData, 'email', false)){
                ret['success'] = true;
                ret['data']    = userData;
                ret['message'] = '';
            }
        }
    }
    catch(err){
        ret['message'] = err;
    }
    finally{ 
        return ret;
    }

}

module.exports = { genToken, verifyToken};