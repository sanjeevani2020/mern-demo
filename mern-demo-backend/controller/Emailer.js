const path      = require('path');
const cfg       = require('dotenv');
cfg.config({path: path.join(__dirname, '..', '.env')});

const nodemailer = require('nodemailer');

const nodemailerSendgrid = require('nodemailer-sendgrid');


class Emailer {

    transport = null;

    constructor(){
        this.transport = nodemailer.createTransport(
            nodemailerSendgrid({apiKey: process.env.SENDGRID_API_KEY})
        );
    }

    sendMail(){
        const msg = {
            to: 'sh.sanj.g2020ch@gmail.com', 
            from: 'sh.sanj.g2020ch@gmail.com', 
            subject: 'Sending with SendGrid is Fun',
            text: 'and easy to do anywhere, even with Node.js',
            html: '<strong>and easy to do anywhere, even with Node.js</strong>',
          }

        return new Promise((resolve, reject) => {
            this.transport.sendMail(msg, function(error, info){
                if (error) {
                    reject(error);
                } else {
                    resolve(info.response);
                }
            });
        });

    }
}

module.exports = (new Emailer());