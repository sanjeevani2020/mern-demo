const { MongoClient }   = require('mongodb');
const cnfg              = require('dotenv');
const path              = require('path');

const apiUtils          = require('./../routes/apiUtils');
const demoEmailer       = require('../controller/Emailer');

cnfg.config({path : path.join(__dirname, '..', '.env')});

const url = `mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}`;
const client = new MongoClient(url);

class DB {


    async login(data){
        
        const result    = await client.connect();
        let db          = result.db(process.env.DB);
        let collection  = db.collection('users');
        let response    = await collection.findOne({'email': data.user, 'password': data.pass});
        if(response != null) {
            const token = apiUtils.genToken(data);
            let ret = {'data' : data, 'token': token};            
            return ret;

        }
        else return 'Invalid Access!'

    }

    async signup(data){

        const result    = await client.connect();
        let db          = result.db(process.env.DB);
        let collection  = db.collection('users');
        let response    = await collection.insertOne({
            'uname'     : data.uname,
            'email'     : data.email,
            'password'  : data.password,
            'role'      : data.role
        }, (err, res) => {
            if(err) throw err;
            else{
                // demoEmailer.sendMail()
                //     .then(() => console.log("email sent"))
                //     .catch(err => console.log("Error while emailing ", err));
                return 'Success';
            }
            
        })

    }



}

module.exports = DB;
