const cors          = require('cors');
const express       = require('express');
const bodyParser    = require('body-parser');
const fileUpload    = require('express-fileupload');
const _             = require('lodash');
const path          = require('path');

const cfg = require('dotenv');
cfg.config();

const app = express();

const middlewares = [
    express.static(path.join(__dirname , 'public')),
    express.urlencoded({limit: '250mb', extended: true, parameterLimit: 5000 }),
    bodyParser.json({ limit: '250mb'}),
    bodyParser.urlencoded({ limit : '250mb', extended: true, parameterLimit: 5000 }),
    fileUpload({ createParentPath: true}),
    cors({
        "origin"                : "*",
        "methods"               : "GET, HEAD, PUT, PATCH, POST, DELETE",
        "preflightContinue"     : false,
        "optionsSuccessStatus"  : 204
    })
];

app.use(middlewares);
// app.get('/', (req, res) => {/', (req, res) => {
//     console.log("Ehill")
// })
//     console.log("Ehill")
// })

app.get('/login', require('./routes/user')());
app.post('/signup', require('./routes/user')());
app.post('/upload', require('./routes/user')());

const port = _.get(process.env, 'PORT', 3000);

app.listen(port, (req, res) => {
    console.log(`App running on port ${port}`);
})

